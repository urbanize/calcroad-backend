from application.create_app import create_app

app = create_app()

# run app for debug
if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
