# CalcROAD backend

Backend de la solution CalcROAD comprennant :
 - l'API
 - le contrôleur
 - la documentation concernant le backend et la modélisation de la base de données

---

## Contribution

Pour pouvoir travailler sur ce projet, vous devez avoir installé python3 et pip3.
Vous devez également avoir installé globalement le module virtualenv à l'aide de la commande suivante :
```bash
pip3 install --user virtualenv
```

Lors de la **première utilisation**, créez un environnement virtuel et installez y les dépendances à l'aide de la commande :
```bash
make install
```

Pour lancer le projet dans l'environement précédement créé, lancez la commande
```bash
make serve
```