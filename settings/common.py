APP_NAME = 'CalcROAD backend'
APP_DESCRIPTION = "A simple road trafic simulator."
APP_VERSION = '2.9'
APP_CONTACT = "urbanize.contact@gmail.com"
APP_SEND_WELCOME = True

# Number of rounds in password hashing algorithm sha256
BCRYPT_LOG_ROUNDS = 10

# Lifetime (format = python.timedelta)
JWT_LIFETIME = {'hours': 24}

# flask mail config
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USERNAME = "noreply.urbanize@gmail.com"
MAIL_PASSWORD = "kotoxoqhaicqrqqn"
MAIL_DEFAULT_SENDER = ("Urbanize", "noreply.urbanize@gmail.com")

SECRET_KEY = 'iv!mjy93aysyz02z4m8doam?tqsckq,6cv-cq-79mb0j!?'

# sqlalchemy config
SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
SQLALCHEMY_TRACK_MODIFICATIONS = False
