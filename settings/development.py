CORS_ALLOWED_ORIGINS = [
    "*", # autorise toutes les requetes
]

# front urls config
FRONTEND_BASE_URL = 'http://localhost:8080'
FRONTEND_CHANGE_PASSWORD_URL = FRONTEND_BASE_URL + '/password/change'
