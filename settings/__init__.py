import os

FLASK_ENV = os.getenv('FLASK_ENV', 'development')
print('FLASK_ENV =', FLASK_ENV)

from .common import *

if FLASK_ENV == 'development':
    from .development import *
elif FLASK_ENV == 'production':
    from .production import *
elif FLASK_ENV == 'integration':
    from .integration import *
elif FLASK_ENV == 'test':
    from .test import *
else:
    raise ValueError("Unknown environment FLASK_ENV=%s"%FLASK_ENV)
