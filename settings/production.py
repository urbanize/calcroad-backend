CORS_ALLOWED_ORIGINS = [
    # autorise seulement les requetes issues du frontend de production
    'http://calcroad.sylvan.ovh/*'
]

# front urls config
FRONTEND_BASE_URL = 'http://calcroad.sylvan.ovh'
FRONTEND_CHANGE_PASSWORD_URL = FRONTEND_BASE_URL + '/password/change'
