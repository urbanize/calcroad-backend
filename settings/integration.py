CORS_ALLOWED_ORIGINS = [
    "http://localhost:8080/*",  # autorise les requetes pour debugage du front
    # autorise les requetes depuis le front d'integration
    "http://calcroad.dev.sylvan.ovh/*"
]

# front urls config
FRONTEND_BASE_URL = 'http://calcroad.dev.sylvan.ovh'
FRONTEND_CHANGE_PASSWORD_URL = FRONTEND_BASE_URL + '/password/change'

#SQLALCHEMY_DATABASE_URI = "postgresql://urbanize:enssat2019@localhost/urbanize_preprod"
