APP_SEND_WELCOME = False

# aucune requete cors ne doit être effectuée pour tester le backend
CORS_ALLOWED_ORIGINS = []

# front urls config
FRONTEND_BASE_URL = 'http://localhost:8080'
FRONTEND_CHANGE_PASSWORD_URL = FRONTEND_BASE_URL + '/password/change'
