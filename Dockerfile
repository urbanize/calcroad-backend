# This file is a template, and might need editing before it works on your project.
FROM python:3.6

ARG FLASK_ENV
ENV FLASK_ENV=$FLASK_ENV
WORKDIR /usr/src/app

COPY requirements/ /usr/src/app/requirements/
RUN echo $FLASK_ENV
RUN pip install --no-cache-dir -r requirements/prod.txt

COPY . /usr/src/app

# Run WSGI application with gunicorn
EXPOSE 80
CMD ["gunicorn", "-b", "0.0.0.0:80", "run:app"]
