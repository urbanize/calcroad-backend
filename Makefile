.PHONY: help

help: ## display this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

	
lazy-install: ## install project dependencies in existing virtualenv
	export FLASK_ENV=development && .env/bin/pip install -r requirements/dev.txt

install: ## (re)create virtualenv and install project dependencies
	rm -rf .env
	virtualenv .env -p python3
	make lazy-install

lazy-test: ## run test using existing test virtualenv
	export FLASK_ENV=test && .test-env/bin/python -m pytest -rA -s -W ignore::DeprecationWarning

test: ## run a complete test
	rm -rf .test-env
	virtualenv .test-env -p python3
	.test-env/bin/pip install -r requirements/test.txt > /dev/null
	make lazy-test

serve: ## run project in debug mode
	.env/bin/python run.py
