from .roads import RoadSchema, Road

roads_schema = RoadSchema(many=True)


class GeoJSON:
    @classmethod
    def dump_roads(cls, roads):
        return [{
            "type": "Feature",
            "properties": {
                "id": road_data['id']
            },
            "geometry": {
                "type": "LineString",
                "coordinates": road_data['coordinates']
            }
        } for road_data in roads_schema.dump(roads)]


    @classmethod
    def dump(cls, map):
        return {
            "type": "FeatureCollection",
            "features": [
                *cls.dump_roads(map.roads)
            ]
        }
