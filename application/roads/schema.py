from marshmallow import fields
from ..addons import ma
from ..point import PointField
from .model import Road


class RoadSchema(ma.SQLAlchemySchema):
    coordinates = fields.List(PointField())

    class Meta:
        fields = ('id', 'name', 'max_speed', 'lanes', 'coordinates')
        model = Road
        strict = True
        load_instance = True
