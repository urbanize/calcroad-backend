from flask_restx import fields
from ..addons import api
from ..point import ApiPointField

RoadApiModel = api.model('Road', {
    'id': fields.Integer(),
    'name': fields.String(required=True),

    'coordinates': fields.List(ApiPointField(), required=True),
    'max_speed': fields.Integer(required=True),
    'lanes': fields.Integer(required=True)
})
