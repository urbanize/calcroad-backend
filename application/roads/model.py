from sqlalchemy.orm import backref
from ..addons import db
from ..point import Point
from ..framework import HttpMixin

# database associations
point_road_association_table = db.Table('point_road_association', db.metadata,
    db.Column('point_id', db.Integer, db.ForeignKey('point.id')),
    db.Column('road_id', db.Integer, db.ForeignKey('road.id'))
)

class Road(HttpMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'), nullable=False)
    
    name = db.Column(db.String, nullable=False)
    max_speed = db.Column(db.Integer, nullable=False)
    lanes = db.Column(db.Integer, nullable=False, default=1)
    
    coordinates = db.relationship("Point", 
        secondary=point_road_association_table,
        backref=backref(
            'point', 
            remote_side=[id], 
            cascade="all, delete-orphan", 
            single_parent=True
        ),
        order_by=Point.id
    )
