from flask_restx import fields
from ..addons import api
from ..point import ApiPointField


class TimeField(fields.Raw):
    __schema__ = {
        'type': 'time',
        'format': 'hh:mm:ss.millisec',
        'example': '13:31:33.280905'
    }


JourneyApiModel = api.model('Journey', {
    "id": fields.Integer(),
    "name": fields.Integer(),
    "start_point": ApiPointField(),
    "end_point": ApiPointField(),
    "start_time": TimeField(),
    "max_vehicules": fields.Integer()
})
