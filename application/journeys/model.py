from ..addons import db
from ..point import Point
from ..framework import HttpMixin
from datetime import datetime

class Journey(HttpMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String, nullable=False)
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'))
    
    max_vehicules = db.Column(db.Integer, default=1)
    
    start_point_id = db.Column(db.ForeignKey('point.id'), nullable=False)
    start_point = db.relationship(Point, cascade="all, delete-orphan", single_parent=True, foreign_keys=[start_point_id])
    
    end_point_id = db.Column(db.ForeignKey('point.id'), nullable=False)
    end_point = db.relationship(Point, cascade="all, delete-orphan", single_parent=True, foreign_keys=[end_point_id])

    start_time = db.Column(db.Integer, nullable=False)