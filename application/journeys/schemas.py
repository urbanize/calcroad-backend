from marshmallow import fields, validate
from ..point import PointField
from ..addons import ma
from .model import Journey


class JourneySchema(ma.SQLAlchemySchema):
    start_point = PointField(required=True)
    end_point = PointField(required=True)
    start_time = fields.Integer(
        required=True, validate=validate.Range(min=0, max=86400))

    class Meta:
        fields = ('id', 'name', 'start_point', 'end_point',
                  'start_time', 'max_vehicules')
        model = Journey
        strict = True
        load_instance = True
