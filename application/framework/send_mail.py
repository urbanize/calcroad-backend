from jinja2 import Environment, FileSystemLoader, select_autoescape
from flask_mail import Message
from ..addons import mail
import settings
import os

mail_environment = Environment(
    loader=FileSystemLoader(searchpath="application/templates/mails"),
)

mail_environment.globals['APP'] = settings

def send_mail(template_name, subject, recipients, **context):
    template = mail_environment.get_template(template_name)
    rendered = template.render(**context)

    msg = Message(subject=subject, recipients=recipients, html=rendered)
    
    mail.send(msg)
