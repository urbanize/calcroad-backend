import json
from flask import jsonify

def insomnia_specs():
    return jsonify(
        json.load(open('docs/resources/insomnia.json'))
    )
