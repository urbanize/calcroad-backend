from .exceptions import abort
from ..addons import db


class HttpMixin:
    @classmethod
    def get_by_id(cls, id):
        obj = cls.query.filter_by(id=id).first()
        if obj is None:
            abort("Can't find %s with id %s"%(cls.__name, id))
        return obj
    
    
    @classmethod
    def get_or_404(cls, id=None, **params):
        if id is not None:
            params['id'] = id
        
        obj = cls.query.filter_by(**params).first() if params else None
        if obj is None:
            abort("%s not found for query %s"%(cls.__name__, params), 404)
        else:
            return obj


    @classmethod
    def search(cls, *filters):
        filters = [filter for filter in filters if filter is not None]
        return cls.query.filter(*filters).all()
    
    
    def create(self):
        db.session.add(self)
        db.session.commit()


    def delete(self):
        db.session.delete(self)
        db.session.commit()


    def commit(self):
        db.session.commit()
