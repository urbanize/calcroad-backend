import marshmallow
import sqlalchemy
from functools import wraps

from flask import Response, abort as flask_abort
from json import dumps as json_dump


def abort(json_or_string, status_code=200, headers={}):
    json = {'message': json_or_string} if type(json_or_string) == str else json_or_string
    headers = {'Content-Type': 'application/json', **headers}
    response = Response(json_dump(json), status_code, headers)
    flask_abort(response)


def http_error_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except sqlalchemy.exc.IntegrityError as e:
            message = str(e.orig)
            status_code = 409 if "UNIQUE constraint failed" in message else 500
            return {"message": message}, status_code
        except marshmallow.exceptions.ValidationError as e:
            return e.messages, 400
    return wrapper
