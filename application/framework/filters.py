
def StringContainsFilter(column, value, pattern='%%%s%%', insensitive=True):
    print(column, value)
    if value is None:
        return None
    
    if insensitive:
        return column.ilike(pattern % value)
    else:
        return column.like(pattern % value)
    
def StringBeginWithFilter(column, value, insensitive=True):
    return StringContainsFilter(column, value, pattern='%s%%', insensitive=insensitive)
