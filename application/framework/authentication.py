from sqlalchemy import Column, String
from sqlalchemy.ext.hybrid import hybrid_property
from flask_jwt_extended import (
    create_access_token, create_refresh_token, jwt_required, 
    jwt_refresh_token_required, current_user)

from functools import wraps
import datetime

from settings import BCRYPT_LOG_ROUNDS, JWT_LIFETIME
from ..addons import bcrypt, api
from .exceptions import abort


class UserMixin:
    email = Column(String, unique=True, nullable=False)
    pwd_hash = Column(String, nullable=False)

    def generate_token(self):
        expires = datetime.timedelta(**JWT_LIFETIME)
        access_token = create_access_token(identity=self.id, expires_delta=expires)
        refresh_token = create_refresh_token(identity=self.id)
        return {
            'access_token': access_token,
            'refresh_token': refresh_token,
            'expires': int(expires.total_seconds())
        }

    @hybrid_property
    def password(self):
        return None

    @password.setter
    def password(self, passphrase):
        self.pwd_hash = bcrypt.generate_password_hash(
            passphrase, BCRYPT_LOG_ROUNDS
        ).decode()

    @classmethod
    def authenticate(cls, email, password, validate=True):
        user = cls.query.filter_by(email=email).first()
        if user is not None and bcrypt.check_password_hash(user.pwd_hash, password):
            return user
        elif validate:
            abort("User authentication failed", 401)
        else:
            return None


def token_required(func):
    @api.doc(security='apikey')
    @api.header('Authorization: Bearer', 'JWT TOKEN', required=True)
    @jwt_required
    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


def refresh_token_required(func):
    @jwt_refresh_token_required
    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper
