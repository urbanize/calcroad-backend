from flask_restx import fields
from ..addons import api


SimulationStateApiModel = api.model('SimulationState', {
    'code': fields.Integer(required=True, help='0: simulation ready, 1: simulation required'),
    'message': fields.String(required=True)
})
