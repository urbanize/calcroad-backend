from sqlalchemy.orm import backref
from flask_jwt_extended import current_user

from ..addons import db
from ..point import Point
from ..roads import Road
from ..users import UserMapPermission
from ..journeys import Journey
from ..framework import HttpMixin
from ..framework.filters import StringBeginWithFilter

class Map(HttpMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String, unique=True, nullable=False)
    public = db.Column(db.Boolean, default=False)
    
    center_point_id = db.Column(db.ForeignKey('point.id'))
    center = db.relationship(Point, cascade="all, delete-orphan", single_parent=True)

    roads = db.relationship(Road, cascade="all, delete-orphan", single_parent=True)
    journeys = db.relationship(Journey)
    
    # property that indicate whether simulation should be restarted
    changed = db.Column(db.Boolean, default=True)
    
    
    def notifyChange(self, value=True):
        self.changed = value
        self.commit()
    
    
    def add_user(self, user_id, read=True, write=False, owner=False):
        return UserMapPermission.create_or_update(
            user_id, 
            self.id, 
            read=read, 
            write=write, 
            owner=owner
        )
    
    @classmethod
    def search(cls, title=None, scope='read'):
        print('search map', title, scope)
        return super().search(
            # map on which current user has permission        
            UserMapPermission.user_id == current_user.id,
            UserMapPermission.map_id == Map.id,
            # permission required
            getattr(UserMapPermission, scope) == True,
            # filter by title
            StringBeginWithFilter(cls.title, title)
        )
