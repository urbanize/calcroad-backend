from functools import wraps
from flask_jwt_extended import current_user, jwt_required
from .model import Map
from ..users import UserMapPermission
from ..addons import api
from ..framework import abort


def check_permission(user_id, map_id, read=False, write=False, owner=False):
    permission = UserMapPermission.query.filter_by(user_id=user_id, map_id=map_id).first()
    
    errors = dict(error for triggered, error in (
        (
            permission is None or permission.read < read,
            ("read", "you don't have the right to read this map")
        ), (
            permission is None or permission.write < write,
            ("write", "you don't have the right to write on this map")
        ), (
            permission is None or permission.owner < owner, 
            ("owner", "you don't have owner privileges on this map")
        )
    ) if triggered == True)
    
    if errors:
        abort({'errors': errors}, 403)


def map_permission(read=False, write=False, owner=False):
    """
    decorator that assert current user has required authorization for the current map
    if not, http 403 is raised
    """
    def external_wrapper(func):
        @wraps(func)
        @api.doc(security="apikey", responses={
            401: 'Unauthorized', 
            403: 'Access forbidden [authorization(s) required: %s]'%','.join(
                [string for string in ('read'*read, 'write'*write, 'owner'*owner) if string]), 
            404: 'Map or object not found'
        })
        @jwt_required
        def wrapper(*args, **kwargs):
            # retrieve map_id from url params
            map_id = kwargs.get('map_id')
            # assert that map exists
            map = Map.get_or_404(map_id)
            # abort with response 403 if permissions doesn't match requirements
            check_permission(current_user.id, map_id, read=read, write=write, owner=owner)
            response = func(*args, **kwargs)
            # if query write on this map, indicate that it changed
            if write: map.notifyChange()
            # return response of the view
            return response
        return wrapper
    return external_wrapper
