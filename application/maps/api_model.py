from flask_restx import fields
from ..addons import api
from ..point import ApiPointField
from ..journeys import JourneyApiModel
from ..roads import RoadApiModel


MapApiModel = api.model('Map', {
    "id": fields.Integer(),
    "title": fields.String(required=True),
    "center": ApiPointField(required=True),
    "public": fields.Boolean(),
    "journeys": fields.List(fields.Nested(JourneyApiModel)),
    "roads": fields.List(fields.Nested(RoadApiModel))
})

MapSimpleApiModel = api.model('MapSimple', {
    "id": fields.Integer(),
    "title": fields.String(required=True),
    "center": ApiPointField(required=True),
    "public": fields.Boolean()
})
