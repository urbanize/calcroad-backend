from flask_restx.reqparse import RequestParser


map_query_parser = RequestParser()

map_query_parser.add_argument('title', type=str)
map_query_parser.add_argument('scope', type=str, default='read',
                              choices=('read', 'write', 'owner'),
                              help='display maps on which current user has required permissions')
