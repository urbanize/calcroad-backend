from marshmallow import fields
from ..point import PointField
from ..roads import RoadSchema
from ..journeys import JourneySchema
from ..addons import ma
from .model import Map, UserMapPermission


class MapSimpleSchema(ma.SQLAlchemySchema):
    center = PointField()

    class Meta:
        fields = ('id', 'title', 'center', 'public')
        model = Map
        strict = True
        load_instance = True


class MapSchema(ma.SQLAlchemySchema):
    center = PointField()
    roads = fields.Nested(RoadSchema, many=True)
    journeys = fields.Nested(JourneySchema, many=True)

    class Meta:
        fields = ('id', 'title', 'center', 'public', 'roads', 'journeys')
        model = Map
        strict = True
        load_instance = True
