from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix

import settings
from .framework import insomnia_specs
from .addons import bcrypt, cors, jwt, api, mail, db, ma
from .endpoints import register_namespaces
from .populate import populate_database


def create_app():
    app = Flask(__name__)
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.config.from_object(settings)

    bcrypt.init_app(app)
    cors.init_app(app)
    jwt.init_app(app)

    mail.init_app(app)
    api.init_app(app)
    db.init_app(app)
    ma.init_app(app)

    register_namespaces(api)

    with app.app_context():
        if settings.FLASK_ENV == 'production':
            db.drop_all()
        
        db.create_all()
        if settings.FLASK_ENV in ('production', 'integration', 'development'):
            populate_database()

    @app.route('/insomnia.json')
    def insomnia_specs_route():
        return insomnia_specs()
    
    return app
