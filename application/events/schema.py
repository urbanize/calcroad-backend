from marshmallow import fields
from ..addons import ma
from ..point import PointField
from .model import Event


class EventSchema(ma.SQLAlchemySchema):
    coordinates = PointField()

    class Meta:
        fields = ('id', 'map_id', 'coordinates', 'start', 'stop', 'speed')
        model = Event
        load_only = ('map_id',)
        dump_only = ('id',)
        strict = True
        load_instance = True
