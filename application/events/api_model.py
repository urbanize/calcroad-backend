from flask_restx import fields
from ..addons import api
from ..point import ApiPointField

EventApiModel = api.model('Event', {
    'id': fields.Integer(),
    'coordinates': ApiPointField(required=True),
    'start': fields.Integer(required=True),
    'stop': fields.Integer(required=True),
    'speed': fields.Integer(required=True)
})
