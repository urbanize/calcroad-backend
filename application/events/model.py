from ..addons import db
from ..point import Point
from ..framework import HttpMixin


class Event(HttpMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'), nullable=False)
    
    coordinates_id = db.Column(db.Integer, db.ForeignKey('point.id'), nullable=False)
    coordinates = db.relationship('Point')
    
    start = db.Column(db.Integer, nullable=False)
    stop = db.Column(db.Integer, nullable=False)
    speed = db.Column(db.Integer, nullable=False)
    