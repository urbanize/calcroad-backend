from marshmallow import fields
from flask_restx import fields as rpfields
from .model import Point


class ApiPointField(rpfields.Raw):
    __schema__ = {
        'type': 'point',
        'format': 'lat, $lng',
        'example': [48.73333, -3.46667]
    }


class PointField(fields.Field):
    def _serialize(self, point, attr, obj, **kwargs):
        if point is None:
            return None
        return [point.lat, point.lng]

    def _deserialize(self, coords, attr, data, **kwargs):
        point = Point(lat=coords[0], lng=coords[1])
        return point
