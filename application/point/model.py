from ..addons import db

class Point(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    lat = db.Column(db.Float, nullable=False)
    lng = db.Column(db.Float, nullable=False)