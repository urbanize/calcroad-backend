from ..addons import ma
from .model import Point


class PointSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('lat', 'lng')
        model = Point
        strict = True
        load_instance = True
