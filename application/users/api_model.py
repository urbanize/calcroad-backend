from flask_restx import fields
from ..addons import api
from ..examples import (
    EXAMPLE_EMAIL, EXAMPLE_PASSWORD, EXAMPLE_EXPIRATION,
    EXAMPLE_FIRST_NAME, EXAMPLE_LAST_NAME, EXAMPLE_ACCESS_TOKEN,
    EXAMPLE_REFRESH_TOKEN)


CredentialsApiModel = api.model('Credentials', {
    'email': fields.String(
        required=True,
        example=EXAMPLE_EMAIL
    ),
    'password': fields.String(
        required=True,
        example=EXAMPLE_PASSWORD
    )
})

GrantApiModel = api.model('Grant', {
    'access_token': fields.String(
        required=True,
        example=EXAMPLE_ACCESS_TOKEN
    ),
    'refresh_token': fields.String(
        required=True,
        example=EXAMPLE_REFRESH_TOKEN
    ),
    'expires': fields.Integer(
        required=True,
        example=EXAMPLE_EXPIRATION
    ),
})

UserApiModel = api.model('User', {
    'id': fields.Integer(),
    'first_name': fields.String(
        required=True,
        example=EXAMPLE_FIRST_NAME
    ),
    'last_name': fields.String(
        required=True,
        example=EXAMPLE_LAST_NAME
    ),
    'email': fields.String(
        required=True,
        example=EXAMPLE_EMAIL
    )
})

UserWithPasswordApiModel = api.model('UserWithPassword', {
    'first_name': fields.String(
        required=True,
        example=EXAMPLE_FIRST_NAME
    ),
    'last_name': fields.String(
        required=True,
        example=EXAMPLE_LAST_NAME
    ),
    'email': fields.String(
        required=True,
        example=EXAMPLE_EMAIL
    ),
    'password': fields.String(
        required=True,
        example=EXAMPLE_PASSWORD
    )
})
