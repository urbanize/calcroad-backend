from flask_restx.reqparse import RequestParser

user_query_parser = RequestParser()

user_query_parser.add_argument('query', type=str)
user_query_parser.add_argument('first_name', type=str)
user_query_parser.add_argument('last_name', type=str)
user_query_parser.add_argument('email', type=str)
