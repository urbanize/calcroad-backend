from sqlalchemy import or_
from ..addons import db, jwt
from ..framework import HttpMixin, UserMixin
from ..framework.filters import StringBeginWithFilter

class UserMapPermission(HttpMixin, db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'), primary_key=True)
    
    user = db.relationship('User')
    read = db.Column(db.Boolean, default=False)
    write = db.Column(db.Boolean, default=False)
    owner = db.Column(db.Boolean, default=False)
    
    def json(self):
        return {
            'read': self.read,
            'write': self.write,
            'owner': self.owner
        }
    
    @classmethod
    def create_or_update(cls, user_id, map_id, read=False, write=False, owner=False):
        permission = cls.query.filter_by(user_id=user_id, map_id=map_id).first()
        if permission:
            permission.read = read
            permission.write = write
            permission.owner = owner
            permission.commit()
        else:
            permission = cls(user_id=user_id, map_id=map_id, read=read, write=write, owner=owner)
            permission.create()
        return permission


class User(HttpMixin, UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)            
    
    @classmethod
    def search(cls, query=None, first_name=None, last_name=None, email=None):
        filters = (
            StringBeginWithFilter(cls.first_name, first_name),
            StringBeginWithFilter(cls.last_name, last_name),
            StringBeginWithFilter(cls.email, email)
        ) if query is None else (or_(
            StringBeginWithFilter(cls.first_name, query),
            StringBeginWithFilter(cls.last_name, query),
            StringBeginWithFilter(cls.email, query)
        ),)
        return super().search(*filters)



@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    return User.query.filter_by(id=identity).first()

@jwt.user_loader_error_loader
def custom_user_loader_error(identity):
    return {
        "message": "Unauthorized. Verify that you sent access token (bearer token) in authorization header. If so, try to refresh your access token."
    }, 401
