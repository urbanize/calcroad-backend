from marshmallow import fields
from ..addons import ma
from .model import User
import bcrypt


class UserSchema(ma.SQLAlchemySchema):
    class Meta:
        fields = ('id', 'first_name', 'last_name', 'email', 'password')
        model = User
        strict = True
        load_instance = True


class UserMapPermissionSchema(ma.SQLAlchemySchema):
    user = fields.Nested(UserSchema)

    class Meta:
        fields = ('user', 'read', 'write', 'owner')
