from flask import request
from flask_restx import Namespace, Resource
from settings import APP_SEND_WELCOME
from ..framework import send_mail


from ..users import (
    CredentialsApiModel, UserWithPasswordApiModel,
    UserApiModel, GrantApiModel, User, UserSchema,
    user_query_parser)
from ..framework import (
    abort, http_error_handler, current_user,
    token_required, refresh_token_required)

user_schema = UserSchema()
users_schema = UserSchema(many=True)


users_ns = Namespace(
    'Users', 'Endpoints to create, edit or delete users.', path='/')


@users_ns.route('/users')
class BrowseUsersResource(Resource):
    @users_ns.doc(parser=user_query_parser, model=[UserApiModel])
    @token_required
    def get(self):
        """
        Browse users
        """
        query_params = user_query_parser.parse_args()
        users = User.search(**query_params)
        return users_schema.dump(users)


@users_ns.route('/users/me')
class CurrentUserResource(Resource):
    @users_ns.marshal_with(UserApiModel)
    @token_required
    def get(self):
        """
        Get authenticated user's details
        """
        return current_user


@users_ns.route('/users/account')
class UsersResource(Resource):
    @users_ns.doc(security=None, body=UserWithPasswordApiModel, model=UserApiModel)
    @http_error_handler
    def post(self):
        """
        Create user account
        """
        new_user = user_schema.load(request.json)
        new_user.create()

        if APP_SEND_WELCOME:
            send_mail(
                'welcome.html',
                'Welcome to CalcROAD %s!' % new_user.first_name,
                recipients=[new_user.email]
            )

        return user_schema.dump(new_user), 201


@users_ns.route('/users/account/<int:user_id>')
class UsersResource(Resource):
    @users_ns.doc(model=UserApiModel)
    @token_required
    @http_error_handler
    def delete(self, user_id):
        """
        Delete user account
        """
        if current_user.id != user_id:
            abort("You can't delete someone else's user account", 403)

        user = User.get_or_404(user_id)
        user.delete()
        return user_schema.dump(user), 200


@users_ns.route('/auth/token')
class AccessTokenResource(Resource):
    @users_ns.expect(CredentialsApiModel, validate=True)
    @users_ns.doc(model=GrantApiModel)
    def post(self):
        """
        Obtain access token by sending credentials.
        """
        user = User.authenticate(
            request.json['email'], request.json['password'])
        return user.generate_token()
