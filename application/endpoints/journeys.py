from flask import request
from flask_restx import Namespace, Resource

from ..framework import http_error_handler, token_required
from ..maps import Map, map_permission

from ..journeys import Journey, JourneySchema, JourneyApiModel

journey_schema = JourneySchema()
journeys_schema = JourneySchema(many=True)

journeys_ns = Namespace(
    'Journeys', 'Endpoints to create edit or delete journeys', path='/')


@journeys_ns.route('/maps/<int:map_id>/journeys')
class MapJourneysResource(Resource):
    @journeys_ns.doc(model=[JourneyApiModel])
    @map_permission(read=True)
    def get(self, map_id):
        """
        Get journeys of a map
        """
        journeys = Journey.query.filter_by(map_id=map_id).all()
        return journeys_schema.dump(journeys)

    @journeys_ns.doc(body=JourneyApiModel, model=JourneyApiModel)
    @map_permission(write=True)
    def post(self, map_id):
        """
        Create a journey on a given map
        """
        new_journey = journey_schema.load(request.json)
        new_journey.map_id = map_id
        new_journey.create()
        return journey_schema.dump(new_journey), 201


@journeys_ns.route('/maps/<int:map_id>/journeys/<int:journey_id>')
class MapJourneyResource(Resource):
    @journeys_ns.doc(model=JourneyApiModel)
    @map_permission(read=True)
    def get(self, map_id, journey_id):
        """
        Browse journeys of a map
        """
        journey = Journey.get_or_404(id=journey_id, map_id=map_id)
        return journey_schema.dump(journey)

    @journeys_ns.doc(body=JourneyApiModel, model=JourneyApiModel)
    @map_permission(write=True)
    def put(self, map_id, journey_id):
        """
        Edit a journey
        """
        journey = journey_schema.load(
            request.json,
            partial=True,
            instance=Journey.get_or_404(
                journey_id,
                map_id=map_id
            )
        )
        journey.commit()
        return journey_schema.dump(journey)

    @journeys_ns.doc(model=JourneyApiModel)
    @map_permission(write=True)
    def delete(self, map_id, journey_id):
        """
        Delete a journey
        """
        journey = Journey.get_or_404(journey_id, map_id=map_id)
        journey.delete()
        return journey_schema.dump(journey)
