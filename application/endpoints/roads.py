from flask import request
from flask_restx import Namespace, Resource

from ..framework import http_error_handler, token_required
from ..roads import RoadSchema, Road, RoadApiModel
from ..maps import Map, map_permission

road_schema = RoadSchema()
roads_schema = RoadSchema(many=True)


roads_ns = Namespace(
    'Roads', 'Endpoints to create, edit or delete roads.', path='/')


@roads_ns.route('/maps/<int:map_id>/roads')
class RoadsResource(Resource):
    @roads_ns.doc(model=[RoadApiModel])
    @map_permission(read=True)
    def get(self, map_id):
        """
        Get roads for a given map
        """
        roads = Road.query.filter_by(map_id=map_id).all()
        return roads_schema.dump(roads)

    @http_error_handler
    @map_permission(write=True)
    def post(self, map_id):
        """
        Create road on a given map
        """
        new_road = road_schema.load(request.json)

        map = Map.query.filter_by(id=map_id).first()

        if map is None:
            return {"message": "Map with id %s not found" % map_id}, 404

        new_road.map_id = map_id
        new_road.create()
        return road_schema.dump(new_road), 201


@roads_ns.route('/maps/<int:map_id>/roads/<int:road_id>')
class RoadResource(Resource):
    @roads_ns.doc(model=[RoadApiModel])
    @map_permission(read=True)
    def get(self, map_id, road_id):
        """
        Get a road
        """
        road = Road.get_or_404(road_id, map_id=map_id)
        return road_schema.dump(road)

    @roads_ns.doc(body=RoadApiModel, model=RoadApiModel)
    @map_permission(write=True)
    def put(self, map_id, road_id):
        """
        Edit a road
        """
        road = road_schema.load(request.json, instance=Road.get_or_404(
            road_id, map_id=map_id), partial=True)
        road.commit()
        return road_schema.dump(road)

    @roads_ns.doc(model=RoadApiModel)
    @map_permission(write=True)
    def delete(self, map_id, road_id):
        """
        Delete a road
        """
        road = Road.get_or_404(road_id, map_id=map_id)
        road.delete()
        return road_schema.dump(road)
