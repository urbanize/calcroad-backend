from .users import users_ns

from .maps import maps_ns
from .roads import roads_ns
from .journeys import journeys_ns
from .events import events_ns

from .simulation import simulation_ns

def register_namespaces(api):
    api.add_namespace(users_ns)
    
    api.add_namespace(maps_ns)
    api.add_namespace(roads_ns)
    api.add_namespace(journeys_ns)
    api.add_namespace(events_ns)
    
    api.add_namespace(simulation_ns)
