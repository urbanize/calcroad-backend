from flask import request
from flask_restx import Namespace, Resource

from ..framework import http_error_handler
from ..events import Event, EventApiModel, EventSchema
from ..maps import Map, map_permission

events_ns = Namespace(
    'Events', 'Endpoints to create edit or delete journeys', path='/')

event_schema = EventSchema()
events_schema = EventSchema(many=True)


@events_ns.route('/maps/<int:map_id>/events')
class EventsResource(Resource):
    @map_permission(read=True)
    @events_ns.doc(model=[EventApiModel])
    def get(self, map_id):
        """
        List events of a map
        """
        events = Event.query.filter_by(map_id=map_id).all()
        return events_schema.dump(events)

    @map_permission(write=True)
    @http_error_handler
    @events_ns.doc(body=EventApiModel, model=EventApiModel)
    def post(self, map_id):
        """
        Create event on a given map
        """
        data = request.json or {}
        data['map_id'] = map_id
        new_event = event_schema.load(request.json)
        new_event.create()
        return event_schema.dump(new_event), 201


@events_ns.route('/maps/<int:map_id>/events/<int:event_id>')
class EventResource(Resource):
    @map_permission(read=True)
    @events_ns.doc(model=EventApiModel)
    def get(self, map_id, event_id):
        """
        Get event details
        """
        event = Event.get_or_404(event_id, map_id=map_id)
        return event_schema.dump(event)

    @events_ns.doc(body=EventApiModel, model=EventApiModel)
    def put(self, map_id, event_id):
        """
        Edit event
        """
        event = Event.get_or_404(event_id, map_id=map_id)
        updated_event = event_schema.load(
            request.json, instance=event, partial=True)
        updated_event.commit()
        return event_schema.dump(updated_event)

    @events_ns.doc(model=EventApiModel)
    def delete(self, map_id, event_id):
        """
        Delete event
        """
        event = Event.get_or_404(event_id, map_id=map_id)
        event_data = event_schema.dump(event)
        event.delete()
        return event_data
