from flask import request
from flask_restx import Namespace, Resource
from flask_jwt_extended import current_user

from ..framework import http_error_handler, token_required
from ..maps import Map, MapSchema, MapSimpleSchema, map_permission, map_query_parser, MapApiModel, MapSimpleApiModel
from ..geojson import GeoJSON
from ..users import UserMapPermission, User, UserSchema, User, UserMapPermissionSchema
from ..addons import db

map_schema = MapSchema()
maps_schema = MapSimpleSchema(many=True)
user_map_permission_schema = UserMapPermissionSchema()
user_map_permissions_schema = UserMapPermissionSchema(many=True)


def create_map(data):
    new_map = map_schema.load(data)
    new_map.create()

    # map creator has all permissions
    new_map.add_user(
        current_user.id,
        read=True,
        write=True,
        owner=True
    )

    return new_map


maps_ns = Namespace(
    'Maps', 'Endpoints to create, edit or delete maps. Dump as GeoJSON.', path='/')


@maps_ns.route('/maps')
class MapsResource(Resource):
    @maps_ns.doc(parser=map_query_parser, model=MapSimpleApiModel)
    @token_required
    def get(self):
        """
        Browse maps
        """
        query_params = map_query_parser.parse_args()
        maps = Map.search(**query_params)
        return maps_schema.dump(maps)

    @http_error_handler
    @maps_ns.doc(body=MapSimpleApiModel, model=MapApiModel)
    @token_required
    def post(self):
        """
        Create a map
        """
        new_map = create_map(request.json)
        return map_schema.dump(new_map), 201


@maps_ns.route('/maps/<int:map_id>')
class MapResource(Resource):
    @maps_ns.doc(model=MapApiModel)
    @map_permission(read=True)
    def get(self, map_id):
        """
        Retrieve a map's details
        """
        map = Map.get_or_404(map_id)
        return map_schema.dump(map)

    @maps_ns.doc(model=MapApiModel)
    @map_permission(owner=True)
    def put(self, map_id):
        """
        Edit a map's details
        """
        map = Map.get_or_404(map_id)
        update_map = map_schema.load(request.json, instance=map, partial=True)
        update_map.commit()
        return map_schema.dump(update_map)

    @map_permission(owner=True)
    def delete(self, map_id):
        """
        Delete a map
        """
        map = Map.get_or_404(map_id)
        map.delete()
        return map_schema.dump(map)


@maps_ns.route('/maps/<int:map_id>/users')
class MapUsersResource(Resource):
    @map_permission(read=True)
    def get(self, map_id):
        """
        List user's of a map and their permissions
        """
        permissions = UserMapPermission.query.filter_by(map_id=map_id).all()
        return user_map_permissions_schema.dump(permissions)

    @map_permission(owner=True)
    def post(self, map_id):
        """
        Add a user to a map with given permissions
        """
        map = Map.get_or_404(map_id)
        user_id = request.json.get('user_id')
        user = User.get_or_404(user_id)

        read = request.json.get('read', False)
        write = request.json.get('write', False)
        owner = request.json.get('owner', False)

        # add right to read and write to new user
        permission = map.add_user(user.id, read, write, owner)

        return user_map_permission_schema.dump(permission)


@maps_ns.route('/maps/<int:map_id>/users/<int:user_id>')
class MapUsersResource(Resource):
    @map_permission(read=True)
    def get(self, map_id, user_id):
        """
        Get user's permission for a map
        """
        permission = UserMapPermission.get_or_404(
            map_id=map_id, user_id=user_id)
        return user_map_permission_schema.dump(permission)

    @map_permission(owner=True)
    def put(self, map_id, user_id):
        """
        Edit user permission for a map
        """
        user = User.get_or_404(user_id)
        permission = UserMapPermission.get_or_404(
            map_id=map_id, user_id=user_id)

        permission = user_map_permission_schema.load(
            request.json, instance=permission)
        permission.commit()

        return user_map_permission_schema.dump(permission)

    @map_permission(owner=True)
    def delete(self, map_id, user_id):
        """
        Delete user permission for a map
        """
        permission = UserMapPermission.get_or_404(
            map_id=map_id, user_id=user_id)
        permission_data = user_map_permission_schema.dump(permission)
        permission.delete()
        return permission_data


@maps_ns.route('/maps/geojson/<int:map_id>')
class RoadResource(Resource):
    @map_permission(read=True)
    def get(self, map_id):
        """
        Dump a map as geojson
        """
        map = Map.get_or_404(map_id)
        return GeoJSON.dump(map)
