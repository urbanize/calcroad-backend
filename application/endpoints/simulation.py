from flask_restx.reqparse import RequestParser
from flask_restx import Namespace, Resource
from calcroad_simulation import Simulation, SimulationStream
from ..simulation import SimulationStateApiModel
from ..maps import Map
from ..addons import db, VehiculePosition

simulation_ns = Namespace(
    'Simulation', 'Run simulation, get report, retrieve details by chunks, delete data...', path='/')


def simulation_required(map_id):
    return Map.get_or_404(map_id).changed == True


def simulation_state(map_id):
    if simulation_required(map_id):
        return {
            "code": 1,
            "state": "SIMULATION_REQUIRED"
        }
    else:
        return {
            "code": 0,
            "state": "SIMULATION_UP_TO_DATE"
        }


def drop_simulation_data(map_id):
    map = Map.get_or_404(map_id)
    map.notifyChange()
    VehiculePosition.query.filter_by(map_id=map_id).delete()
    db.session.commit()


@simulation_ns.route('/maps/<int:map_id>/simulation')
class SimulationResource(Resource):
    @simulation_ns.doc(model=SimulationStateApiModel)
    def get(self, map_id):
        """
        Get information concerning a simulation
        """
        return simulation_state(map_id)

    @simulation_ns.doc(model=SimulationStateApiModel)
    def post(self, map_id):
        """
        Run simulation if needed
        """
        map = Map.get_or_404(map_id)

        if simulation_required(map_id):
            print('run simulation')

            # delete old simulation data
            drop_simulation_data(map_id)

            # load data from database and convert it to simulation format
            simulation = Simulation(map)

            # run simulation
            simulation.run()
            print('simulation done')

            # notify that simulation is up to date for this map
            map.notifyChange(False)

        return simulation_state(map_id)

    @simulation_ns.doc(model=SimulationStateApiModel)
    def delete(self, map_id):
        """
        Drop existing simulation data
        """
        drop_simulation_data(map_id)
        return simulation_state(map_id)


simulation_parser = RequestParser()
simulation_parser.add_argument(
    'time', type=int, required=True, help='begin time of the chunks to retrieve')
simulation_parser.add_argument(
    'limit', type=int, default=30, help='number of chunks to receive < 50')


@simulation_ns.route('/maps/<int:map_id>/simulation/stream')
class SimulationStreamResource(Resource):
    @simulation_ns.doc(parser=simulation_parser)
    def get(self, map_id):
        """
        Get information concerning a simulation
        """
        if not simulation_required(map_id):
            args = simulation_parser.parse_args()
            return SimulationStream.dump(
                map_id,
                start_time=args.time,
                limit=args.limit
            )
        else:
            return simulation_state(map_id)
