from .users import User
from .maps import Map, MapSimpleSchema
from .roads import Road, RoadSchema
from .journeys import Journey, JourneySchema
from .point import Point
from .addons import db
import json

def create_users():
    baptiste = User(
        first_name='Baptiste', 
        last_name='PRIEUR', 
        email='bprieur@enssat.fr', 
        password='toto'
    )
    sylvan = User(
        first_name='Sylvan', 
        last_name='LE DEUNFF', 
        email='sledeunf@gmail.com', 
        password='toto'
    )
    db.session.add_all((baptiste, sylvan))
    db.session.commit()
    
    return baptiste, sylvan
    

def remove_id(dico):
    dico.pop('id', None)
    return dico

def inject_map_id(map_id):
    def setter(obj):
        setattr(obj, 'map_id', map_id)
    return setter

def load_sample_map(map_title, owners=[]):   
    # load json data in proper format
    map_data = remove_id(json.load(open('samples/'+map_title+'.json')))
    roads_data = map(remove_id, map_data.pop('roads', []))
    journeys_data = map(remove_id, map_data.pop('journeys', []))
    
    # instanciate schemas
    map_schema = MapSimpleSchema()
    roads_schema = RoadSchema(many=True)
    journeys_schema = JourneySchema(many=True)
    
    # instanciate map basic object
    sample_map = map_schema.load(map_data)
    sample_map.create()
    
    # create roads
    roads = roads_schema.load(roads_data)
    tuple(map(inject_map_id(sample_map.id), roads))
    db.session.add_all(roads)
    db.session.commit()
    
    # create journeys
    journeys = journeys_schema.load(journeys_data)
    tuple(map(inject_map_id(sample_map.id), journeys))
    db.session.add_all(journeys)
    db.session.commit()
    
    for owner in owners:
        sample_map.add_user(owner.id, read=True, write=True, owner=True)        
    
    
def populate_database():
    baptiste, sylvan = create_users()
    load_sample_map('brest', owners=[baptiste])
    load_sample_map('saint-nicolas', owners=[baptiste])
    load_sample_map('test-vitesse', owners=[baptiste, sylvan])
    load_sample_map('tokyo-spider', owners=[baptiste, sylvan])
