from settings import JWT_LIFETIME
from datetime import timedelta


EXAMPLE_EMAIL = 'jean.dupont@example.com'
EXAMPLE_FIRST_NAME = 'Jean'
EXAMPLE_LAST_NAME = 'Dupont'
EXAMPLE_PASSWORD = 'dupond'


EXAMPLE_ACCESS_TOKEN = "eyJ0eXAi ... Ol_kTAooiWOI"
EXAMPLE_REFRESH_TOKEN = "eyJ0eXAi ... -SbhhpUMeEf4"
EXAMPLE_EXPIRATION = timedelta(**JWT_LIFETIME).total_seconds()
