# security addons
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_jwt_extended import JWTManager

# helpers addons
from flask_mail import Mail
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from calcroad_simulation import SimulatorSettings

from settings import APP_NAME, APP_DESCRIPTION, APP_VERSION, APP_CONTACT, CORS_ALLOWED_ORIGINS, FLASK_ENV

bcrypt = Bcrypt()
jwt = JWTManager()

cors = CORS(resources={
    "*": {"origins": CORS_ALLOWED_ORIGINS}
})

api = Api(
    title=APP_NAME + f' ({FLASK_ENV})',
    description=APP_DESCRIPTION,
    version=APP_VERSION,
    contact=APP_CONTACT,
    authorizations={
        'apikey': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization',
            'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"
        }
    }
)
jwt._set_error_handler_callbacks(api)

mail = Mail()
db = SQLAlchemy()
ma = Marshmallow()


class VehiculePosition(db.Model):
    __tablename__ = 'vehicule_position'
    id = db.Column(db.String, primary_key=True)
    map_id = db.Column(db.Integer, db.ForeignKey('map.id'), primary_key=True)
    time = db.Column(db.Integer, primary_key=True)

    speed = db.Column(db.Integer, default=50)
    lat = db.Column(db.Float, nullable=False)
    lng = db.Column(db.Float, nullable=False)


SimulatorSettings.from_dict({
    'db_session': db.session,
    'position_model': VehiculePosition
})
