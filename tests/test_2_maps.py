from tests.helpers import authorization, api
from tests.data.maps import lannion, brest
from tests.data.users import sylvan, baptiste

maps = (lannion, brest)

def list_maps():
    return api.get('/maps', headers=authorization(sylvan)).json


def test_create_map():
    for map in maps:
        resp = api.post('/maps', json=map['map_info'], headers=authorization(sylvan))
        assert resp.status_code == 201

        for key, val in map['map_info'].items():
            assert resp.json[key] == val

        map['map_info']['id'] = resp.json['id']


def test_list_maps():
    resp = api.get('/maps', headers=authorization(sylvan))
    assert resp.status_code == 200
    assert len(resp.json) == len(maps)


def test_delete_unexisting_map():
    resp = api.delete('/maps/48548489', headers=authorization(sylvan))
    assert resp.status_code == 404


def test_get_map():
    resp = api.get(f'/maps/{lannion["map_info"]["id"]}', headers=authorization(sylvan))
    assert resp.status_code == 200
    return resp.json

def test_edit_existing_map():
    map_id = lannion["map_info"]["id"]
    resp = api.put(f"/maps/{map_id}", json={"title": "LANNION", "public": True}, headers=authorization(sylvan))
    assert resp.status_code == 200
    assert resp.json["title"] == "LANNION"
    assert resp.json["public"] == True

def test_delete_existing_map():
    map_id = lannion["map_info"]['id']
    resp = api.delete(f'/maps/{map_id}', headers=authorization(sylvan))
    assert resp.status_code == 200


def test_get_deleted_map():
    map_id = lannion["map_info"]['id']
    resp = api.delete(f'/maps/{map_id}', headers=authorization(sylvan))
    assert resp.status_code == 404
