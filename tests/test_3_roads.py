from tests.helpers import authorization, api
from tests.data.maps import brest
from tests.data.users import sylvan, baptiste

def get_roads(map):
    resp = api.get(
        f'/maps/{map["map_info"]["id"]}/roads', 
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    return resp.json


def test_new_map_roads_empty():
    # test that roads are empty for a new map
    roads = get_roads(brest)
    assert len(roads) == 0
    

def test_create_roads():
    # create multiple roads for map brest
    for road_data in brest['roads']:
        resp = api.post(
            f'/maps/{brest["map_info"]["id"]}/roads',
            json=road_data,
            headers=authorization(sylvan)
        )
        assert resp.status_code == 201
        road_data['id'] = resp.json['id']
    
    
def test_roads_created():
    # get roads from map details endpoint
    resp1 = api.get(
        f'/maps/{brest["map_info"]["id"]}', 
        headers=authorization(sylvan)
    )
    assert resp1.status_code == 200
    road_data1 = resp1.json['roads']
    
    # get roads for road endpoint
    resp2 = api.get(
        f'/maps/{brest["map_info"]["id"]}/roads', 
        headers=authorization(sylvan)
    )
    assert resp2.status_code == 200
    road_data2 = resp2.json
    
    # check that results are the same
    assert road_data1 == road_data2
    
    # assert that N roads has been created
    assert len(road_data1) == len(brest['roads'])


def test_edit_road():
    new_road_name = "Two roads"
    
    road = get_roads(brest)[0]
    resp = api.put(
        f'/maps/{brest["map_info"]["id"]}/roads/{road["id"]}',
        json={"name": new_road_name},
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    updated_road = resp.json
    assert updated_road['id'] == road['id']
    assert updated_road['name'] != road['name']
    assert updated_road['name'] == new_road_name    


def test_delete_road():
    # get and count roads for map brest
    roads = get_roads(brest)
    n0 = len(roads)
    
    # delete the first road
    resp = api.delete(
        f'/maps/{brest["map_info"]["id"]}/roads/{roads[0]["id"]}',
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    # count remaining roads
    roads = get_roads(brest)
    n1 = len(roads)
    
    # check that one road has been deleted
    assert n1 == n0-1
