from .api import api

def create_user(user):
    resp = api.post('/users/account', json={
        'password': user['password'], 
        **user['details']
    })
    return resp