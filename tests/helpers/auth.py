from tests.helpers.api import api

def get_access_token(user):
    resp = api.post('/auth/token', json={
        'email': user['details']['email'],
        'password': user['password']
    })
    return resp.json['access_token']

def authorization(user):
    return {'Authorization' : 'Bearer %s'%get_access_token(user)}
