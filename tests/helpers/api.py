from application.create_app import create_app

app = create_app()
api = app.test_client()
