lannion = {
    'map_info': {
        "title": "lannion",
        "public": True,
        "center": [48.724717999999996, -3.44945000000007]
    }
}

brest = {
    'map_info': {
        "title": "brest",
        "center": [48.39039400000002, -4.4860760000000255]
    },
    'roads': [
        {
            "lanes": 1,
            "max_speed": 50,
            "coordinates": [
                [
                    48.38744335669657,
                    -4.493170129974607
                ],
                [
                    48.391374287676875,
                    -4.488451537012946
                ],
                [
                    48.392855429280466,
                    -4.484505077444988
                ],
                [
                    48.392798463092944,
                    -4.482274469863104
                ],
                [
                    48.389494315092335,
                    -4.475668439716759
                ]
            ],
            "name": "Against all roads"
        },
        {
            "lanes": 1,
            "max_speed": 50,
            "coordinates": [
                [
                    48.39729859541123,
                    -4.489098542845417
                ],
                [
                    48.39167990804644,
                    -4.487637229659139
                ],
                [
                    48.391374287676875,
                    -4.488451537012946
                ],
                [
                    48.38883915681181,
                    -4.483822297988274
                ],
                [
                    48.38322723819139,
                    -4.491372046726952
                ],
                [
                    48.38405339703298,
                    -4.492744728315808
                ],
                [
                    48.384879542464894,
                    -4.494288995103274
                ],
                [
                    48.38596205756062,
                    -4.493087898713027
                ],
                [
                    48.38744335669657,
                    -4.493170129974607
                ]
            ],
            "name": "On the road tonight"
        },
        {
            "lanes": 1,
            "max_speed": 100,
            "coordinates": [
                [
                    48.39239969799441,
                    -4.501491755030368
                ],
                [
                    48.394051704486124,
                    -4.49346634358129
                ],
                [
                    48.391374287676875,
                    -4.488451537012946
                ],
                [
                    48.387922848738924,
                    -4.492594579026666
                ],
                [
                    48.384851054914606,
                    -4.48706818623206
                ],
                [
                    48.3866742259881,
                    -4.484536490420048
                ],
                [
                    48.38724395355493,
                    -4.482390985494607
                ],
                [
                    48.38883915681181,
                    -4.483822297988274
                ]
            ],
            "name": "Another road in paradise"
        }
    ],
    "journeys": [
        {
            "name": "New journey",
            "start_time": 28800,
            "start_point": [
                48.392822549438335,
                -4.4994375767645405
            ],
            "end_point": [
                48.392855429280466,
                -4.484505077444988
            ],
            "max_vehicules": 1
        },
        {
            "name": "New Journey",
            "start_time": 28800,
            "start_point": [
                48.385416668800865,
                -4.4936930338560215
            ],
            "end_point": [
                48.390848424429436,
                -4.478375677921652
            ],
            "max_vehicules": 1
        },
        {
            "name": "New Journey",
            "start_time": 28800,
            "start_point": [
                48.38562866319876,
                -4.485988393203649
            ],
            "end_point": [
                48.39518490090438,
                -4.488548792370733
            ],
            "max_vehicules": 1
        }
    ],
    "events": [
        {
            "coordinates": [
                48.39518490090438,
                -4.488548792370733
            ],
            "start": 28800,
            "stop": 29100,
            "speed": 30
        },
        {
            "coordinates": [
                48.385416668800865,
                -4.4936930338560215
            ],
            "start": 29500,
            "stop": 29900,
            "speed": 0
        }
    ]
}