from tests.helpers import api, create_user
from tests.data.users import sylvan, baptiste


jwt = {}

def test_get_token_invalid_credentials():
    resp = api.post('/auth/token', json={
        'email': sylvan['details']['email'],
        'password': sylvan['password']
    })
    assert resp.status_code == 401


def test_create_user_missing_parameters():
    resp = api.post('/users/account', json={
        'email': sylvan['details']['email'],
        'password': sylvan['password']
    })
    assert resp.status_code == 400


def test_create_user():
    resp = create_user(sylvan)
    assert resp.status_code == 201

    for key, val in sylvan['details'].items():
        assert resp.json[key] == val


def test_get_token_valid_credentials():
    resp = api.post('/auth/token', json={
        'email': sylvan['details']['email'],
        'password': sylvan['password']
    })
    assert resp.status_code == 200
    assert resp.json.get('access_token') is not None
    assert resp.json.get('refresh_token') is not None

    jwt['access_token'] = resp.json['access_token']


def test_get_current_user():
    resp = api.get('/users/me', headers={
        'Authorization': 'Bearer %s'%jwt['access_token']
    })
    assert resp.status_code == 200

    for key, val in sylvan['details'].items():
        assert resp.json[key] == val
