from tests.helpers import authorization, api
from tests.data.maps import brest
from tests.data.users import sylvan, baptiste

def get_journeys(map):
    resp = api.get(
        f'/maps/{map["map_info"]["id"]}/journeys',
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    return resp.json

def test_new_map_journeys_empty():
    journeys = get_journeys(brest)
    assert journeys == []
    
def test_create_journeys():
    for journey_data in brest["journeys"]:
        resp = api.post(
            f'/maps/{brest["map_info"]["id"]}/journeys',
            json=journey_data,
            headers=authorization(sylvan)
        )
        assert resp.status_code == 201
    
def test_journeys_created():
    assert len(get_journeys(brest)) == len(brest["journeys"])

def test_edit_journey():
    updated_data = {
        'start_point': [15.3454564, -48.353568]
    }
    
    journey = get_journeys(brest)[0]
    resp = api.put(
        f'/maps/{brest["map_info"]["id"]}/journeys/{journey["id"]}',
        json=updated_data,
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    updated_journey = resp.json
    assert updated_journey['id'] == journey['id']
    for key, val in updated_data.items():
        assert updated_journey[key] == val
    
def test_delete_journey():
    journeys = get_journeys(brest)
    n0 = len(journeys)
    
    resp = api.delete(
        f'/maps/{brest["map_info"]["id"]}/journeys/{journeys[0]["id"]}',
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    journeys = get_journeys(brest)
    n1 = len(journeys)
    
    assert n0-1 == n1
