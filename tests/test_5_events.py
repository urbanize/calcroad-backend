from tests.helpers import authorization, api
from tests.data.maps import brest
from tests.data.users import sylvan, baptiste

def get_events(map):
    resp = api.get(
        f'/maps/{map["map_info"]["id"]}/events',
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    return resp.json

def test_new_map_events_empty():
    events = get_events(brest)
    assert events == []
    
def test_create_events():
    for event_data in brest["events"]:
        resp = api.post(
            f'/maps/{brest["map_info"]["id"]}/events',
            json=event_data,
            headers=authorization(sylvan)
        )
        assert resp.status_code == 201
    
def test_events_created():
    assert len(get_events(brest)) == len(brest["events"])

def test_edit_event():
    updated_data = {
        'coordinates': [
            48.392822549438335,
            -4.4994375767645405
        ],
        'start': 28700
    }
    
    event = get_events(brest)[0]
    resp = api.put(
        f'/maps/{brest["map_info"]["id"]}/events/{event["id"]}',
        json=updated_data,
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    updated_event = resp.json
    assert updated_event['id'] == event['id']
    for key, val in updated_data.items():
        assert updated_event[key] == val
    
def test_delete_event():
    events = get_events(brest)
    n0 = len(events)
    
    resp = api.delete(
        f'/maps/{brest["map_info"]["id"]}/events/{events[0]["id"]}',
        headers=authorization(sylvan)
    )
    assert resp.status_code == 200
    
    events = get_events(brest)
    n1 = len(events)
    
    assert n0-1 == n1
